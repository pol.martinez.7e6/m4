/* EX 1
function estiljs() {
    const text = document.getElementById("text");
    text.style.backgroundColor = "#c410c4";
}
*/

/*
function prenValorForm() {
    var nom = document.getElementsByName('nom')[0].value;
    var cognom = document.getElementsByName("cognom")[0].value;
    console.log(nom, cognom);
}
*/

/*
function pintaParagraf() {
    const contenedor = document.getElementsByTagName("div")
    for (let i = 0; i < contenedor.length; i++) {
        const element = contenedor[i];
        element.style.backgroundColor = "#0a91ab";
    }
}
*/

/*
function obtenirAtributs() {
    const element = document.getElementById("itb");
    const attributes = element.attributes;

    for (let i = 0; i < attributes.length; i++) {
        const attributeValues = attributes[i].value;
        console.log(attributeValues);
    }
}
*/

/*
function insertarFila() {
    var table = document.getElementById("Taula");
    var row = table.insertRow(0);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);

    cell1.innerHTML = "Hola";
    cell2.innerHTML = "Adeu";
}

function insertarUltimaFila() {
    var table = document.getElementById("Taula");
    var row = table.insertRow(-1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);

    cell1.innerHTML = "Hola";
    cell2.innerHTML = "Adeu";
}
*/

const celdas = document.getElementsByTagName("td")

for (let i = 0; i < celdas.length; i++) {
    const element = celdas[i];
    console.log(element)
}