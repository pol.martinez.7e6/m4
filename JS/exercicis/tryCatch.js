var map = L.map('map').setView([41.396645, 2.159276], 13);

L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);

const redMarker = L.icon ({
    iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png',
    iconSize : [15, 25]
});

const yellowMarker = L.icon ({
    iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-yellow.png',
    iconSize : [15, 25]
});

const greenMarker = L.icon ({
    iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
    iconSize : [15, 25]
});

fetch('bicing.json')

   .then(function (response) {
       return response.json();
    })

   .then(function (dataJson) {
    //Hago un array con dataJson y luego hago un filter de las stations que tengan mas de 3 bikes.
    let menosDatos = Array.from(dataJson.stations).filter(station => station.bikes > 0);
        menosDatos.sort(function (a, b) {
            return a.bikes - b.bikes;
        });
        
        //Itero menosDatos que es el array de las bikes > 3 y luego los meto en una table.
        let text = "<table border='1'>"
        text += "<tr><th>Id</th><th>Carrer</th><th>Nº Bicis</th></tr>"
        for (const bici of menosDatos) {
            if (bici.bikes > 10) {
                let marker = L.marker([bici.latitude, bici.longitude], {icon : greenMarker}).addTo(map);
            } else if (bici.bikes >= 3) {
                let marker = L.marker([bici.latitude, bici.longitude], {icon : yellowMarker}).addTo(map);
            } else {
                let marker = L.marker([bici.latitude, bici.longitude], {icon : redMarker}).addTo(map);
            }

            text += "<tr><td>" + bici.id + "</td><td>" + bici.streetName + "</td><td>" + bici.bikes + "</td></tr>";
        }

       text += "</table>"
       document.getElementById("bici").innerHTML = text;
       return dataJson;
    })

   .catch(function(error){console.log("Algo ha salido mal: " + error)})
