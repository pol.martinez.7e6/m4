const codigo = [];
const input = [];
let intento = 0;
let rondas = 1;
let maxIntento = 6;

/*1. Genera una constante CODIGO_SECRETO de tipo array de 5 número aleatorios entre 0 y 9 usando la libreria Math.random();*/
function codigoSecreto() {
    for (let i = 0; i < 5; i++) {
        codigo[i] = Math.floor((Math.random() * 10));
    }
}


function maxIteracion() {
    const resultSection = document.getElementById("Result");

    // Crear un nuevo elemento section
    const newRowResult = document.createElement("div");

    // Agregar el atributo id de "Result" al nuevo elemento section
    newRowResult.setAttribute("class", "rowResult w100 flex wrap");

    // Crear cinco nuevos elementos div con la clase w20
    for (let i = 0; i < 5; i++) {
    const newDiv = document.createElement("div");
    newDiv.classList.add("w20");

    // Crear un nuevo elemento div dentro de cada uno de los nuevos elementos div
    const newCelResult = document.createElement("div");
    newCelResult.classList.add("celResult", "flex");

    // Agregar el nuevo elemento div como hijo del nuevo elemento div
    newDiv.appendChild(newCelResult);

    // Agregar el nuevo elemento div como hijo del nuevo elemento section
    newRowResult.appendChild(newDiv);
    }

    // Agregar el nuevo elemento section como hijo del elemento con el atributo id de "Result"
    resultSection.appendChild(newRowResult);
}


function Comprobar() {
    let numero = document.getElementById("numero").value;
    for (number in numero) {
        input[number] = parseInt(numero[number]);
    }

    if (maxIntento <= 0) {
        const mensaje = document.getElementById("info");
        mensaje.textContent = "No tienes más intentos!";

    } else {
        if (input == codigo) {
            for (let d = 0; d < input.length; d++) {
                const element = input[d];
                
                const mensaje = document.getElementById("info");
                const rowResult1 = document.querySelector(".w100.flex.wrap")[0];
                const casilla = rowResult1.querySelectorAll(".cel.flex")[d];
                console.log(casilla)
                casilla.textContent = element;
                mensaje.textContent = "FELICIDADES HAS ACERTADO EL NUMERO!";
            }
    
        } else {
            const mensaje = document.getElementById("info");
            mensaje.textContent = "Rondas: " + rondas + "/8";
            maxIteracion();
            
            for (let d = 0; d < input.length; d++) {
                
                const inputDigit = input[d];
                const secretDigit = codigo[d]; 
                
                if (inputDigit == secretDigit) {
                    const rowResult = document.querySelectorAll(".rowResult.w100.flex.wrap")[intento];
                    const celResults = rowResult.querySelectorAll(".celResult.flex")[d];
    
                    celResults.textContent = inputDigit;
                    celResults.style.backgroundColor = "green";
    
                } else if (inputDigit != secretDigit) {
                    let igual = false;
                    for (let i = 0; i < codigo.length; i++) {
                        const codigoDigit = codigo[i];
                        if (codigoDigit == inputDigit) {
                            const rowResult = document.querySelectorAll(".rowResult.w100.flex.wrap")[intento];
                            const celResults = rowResult.querySelectorAll(".celResult.flex")[d];
    
                            celResults.textContent = inputDigit;
                            celResults.style.backgroundColor = "yellow";
                            igual = true;
                        }
                    }
                    if (!igual) {
                        const rowResult = document.querySelectorAll(".rowResult.w100.flex.wrap")[intento];
                        const celResults = rowResult.querySelectorAll(".celResult.flex")[d];
    
                        celResults.textContent = inputDigit;
                        celResults.style.backgroundColor = "gray";
                    }
                }
            }
        }
    }
    
    maxIntento--;
    intento++;
    rondas++;
}

codigoSecreto();
console.log(codigo);