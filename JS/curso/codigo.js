/*si creamos un var dentor de un bloque lo podremos utilizar fuera de ese bloque, en cambio si creamos un let dentro de un bloque solo se podra utilizar dentro de ese bloque*/
/*el valor undefined nos dice que la variable esta declarada pero no inicializada*/

/*
let num1 = "2";
let num2 = 1;

let nombre = "Pol Martinez Lopez";
let frase = `Soy ${nombre} y estudio DAWe`;
let numero = num1.concat(num2); /*El resultado de numero es 21, lo mismo pasaria con num1 + num2

console.log(frase);

numero = 23;
let texto = "23";

numero == texto /*devuelve true porque no distingue entre tipos de datos
numero === texto /*devuelve false porque los datos son iguales pero el tipo de dato no

let valor1 = false;
let valor2 = true;

console.log(valor1 || valor2)
*/


/*Recorrer un array utilizando for*/
/*
const pc1 = ["pol", "Intel i7", "8gb ram", "512gb ssd"];

for (let i = 0; i < pc1.length; i++) {
    const element = pc1[i];
    console.log(element)
}

const body = document.body;
console.log(body);
*/

function insert() {
    const list = document.createElement("li");
    const item = document.createTextNode("item");
    list.appendChild(item);
    document.getElementById("myList").appendChild(list);
}

function lastItem() {
    const lista = document.getElementById("myList")
    let item = lista.lastElementChild.innerHTML
    console.log(item)
}

function esfera() {
    let altura = parseFloat(prompt("Introduce la altura:"));
    let volume = 4/3 * Math.PI * Math.pow(altura/2, 3);
    console.log("The volume is: ", volume);
}

esfera()