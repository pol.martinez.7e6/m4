<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match = "/root/prediccion">
    <html>
        <body>
            <table style = "border: solid 1px black">
                <tr style="background-color:lightblue">
                    <th>Fecha</th>
                    <th>Maxima</th>
                    <th>Minima</th>
                    <th>Predicción</th>
                </tr>
                <xsl:for-each select = "dia">
                <xsl:sort order = "descending" select = "temperatura/maxima"/>
                <tr>  
                    <td style = "border: solid 1px black"><xsl:value-of select = "@fecha"></xsl:value-of></td>
                    <td style = "border: solid 1px black"><xsl:value-of select = "temperatura/maxima"></xsl:value-of></td>
                    <td style = "border: solid 1px black"><xsl:value-of select = "temperatura/minima"></xsl:value-of></td>
                    <td>
                        <img>
                            <xsl:attribute name="src">imagenes/<xsl:value-of select="estado_cielo[1]/@descripcion"/>.jpg</xsl:attribute>
                        </img>
                    </td>
                </tr>
                </xsl:for-each>
            </table>
        </body>
    </html>
</xsl:template>
</xsl:stylesheet>