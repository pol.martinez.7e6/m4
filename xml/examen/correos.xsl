<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/correos">
        <html>
            <head>
                <title>Correos</title>
            </head>
            <body>
                <h1>Correos</h1>
                <xsl:apply-templates select="correos" />
                <xsl:choose>
                    <xsl:when test = "carta/@urgente = 'SI'">
                        <table style = "border: solid 5px red">
                                <xsl:for-each select = "carta">
                                    <xsl:sort select = "fechaExpedicion/@dia"/>
                                    <xsl:sort select = "fechaExpedicion/@mes"/>
                                    <xsl:sort select = "fechaExpedicion/@anyo"/>
                                    <tr>
                                    <h2>Destinatario</h2>
                                        <table>
                                            <tr><b>Nombre: </b><xsl:value-of select = "destinatario/nombre"></xsl:value-of></tr><br/><br/>
                                            <tr><b>Direccion: </b><xsl:value-of select = "destinatario/direccion"></xsl:value-of></tr><br/><br/>
                                            <tr><xsl:value-of select = "destinatario/cp"></xsl:value-of></tr><br/><br/>
                                            <tr><xsl:value-of select = "destinatario/ciudad"></xsl:value-of></tr><br/><br/>
                                            <tr><xsl:value-of select = "destinatario/provincia"></xsl:value-of></tr><br/><br/>
                                            <tr><xsl:value-of select = "destinatario/pais"></xsl:value-of></tr>
                                            <tr><p><xsl:value-of select = "fechaExpedicion/@dia"></xsl:value-of>-<xsl:value-of select = "fechaExpedicion/@mes"></xsl:value-of>-<xsl:value-of select = "fechaExpedicion/@anyo"></xsl:value-of></p></tr>
                                        </table>
                                    </tr>
                                    <h2>Remitente</h2>
                                    <tr>
                                        <table>
                                            <tr><b>Nombre: </b><xsl:value-of select = "remite/nombre"></xsl:value-of></tr><br/><br/>
                                            <tr><b>Direccion: </b><xsl:value-of select = "remite/direccion"></xsl:value-of></tr><br/><br/>
                                            <tr><xsl:value-of select = "remite/cp"></xsl:value-of></tr><br/><br/>
                                            <tr><xsl:value-of select = "remite/ciudad"></xsl:value-of></tr><br/><br/>
                                            <tr><xsl:value-of select = "remite/provincia"></xsl:value-of></tr><br/><br/>
                                            <tr><xsl:value-of select = "remite/pais"></xsl:value-of></tr>
                                            <tr><p><xsl:value-of select = "fechaEntrega/@dia"></xsl:value-of>-<xsl:value-of select = "fechaEntrega/@mes"></xsl:value-of>-<xsl:value-of select = "fechaEntrega/@anyo"></xsl:value-of></p></tr>
                                        </table>
                                    </tr>
                                </xsl:for-each>
                        </table>
                    </xsl:when>
                </xsl:choose>
            </body>     
        </html>
    </xsl:template>


    <xsl:template match="XXXXXXXXX">
    </xsl:template>

</xsl:stylesheet>