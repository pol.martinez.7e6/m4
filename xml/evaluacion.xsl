<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/evaluacion">
        <html>
            <body>
                <table style = "border: solid 1px black">
                    <tr style = "background-color: green">
                        <th style = "border: solid 1px black">Nombre</th>
                        <th style = "border: solid 1px black">Apellidos</th>
                        <th style = "border: solid 1px black">Telefono</th>
                        <th style = "border: solid 1px black">Repetidor</th>
                        <th style = "border: solid 1px black">Nota Practicas</th>
                        <th style = "border: solid 1px black">Nota examen</th>
                        <th style = "border: solid 1px black">Nota total</th>
                    </tr>
                    <xsl:for-each select="alumno">
                    <xsl:sort select = "apellidos"/>
                    <tr>
                        <td style = "border: solid 1px black"><xsl:value-of select = "nombre"></xsl:value-of></td>
                        <td style = "border: solid 1px black"><xsl:value-of select = "apellidos"></xsl:value-of></td>
                        <td style = "border: solid 1px black"><xsl:value-of select = "telefono"></xsl:value-of></td>
                        <td style = "border: solid 1px black"><xsl:value-of select = "@repite"></xsl:value-of></td>
                        <td style = "border: solid 1px black"><xsl:value-of select = "notas/practicas"></xsl:value-of></td>
                        <td style = "border: solid 1px black"><xsl:value-of select = "notas/examen"></xsl:value-of></td>
                        <td style = "border: solid 1px black">
                            <p>
                            <xsl:if test="(notas/practicas + notas/examen) div 2 &lt; 5">
                                <xsl:attribute name="class">suspendido</xsl:attribute>
                                <xsl:attribute name="style">background-color: red;</xsl:attribute>
                            </xsl:if>
                            <xsl:if test="(notas/practicas + notas/examen) div 2 &gt; 8">
                                <xsl:attribute name="class">superiores</xsl:attribute>
                                <xsl:attribute name="style">background-color: green;</xsl:attribute>
                            </xsl:if>
                            <xsl:value-of select="(notas/practicas + notas/examen) div 2" />
                            </p>
                        </td>
                    </tr>
                    </xsl:for-each>          
                </table>    
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>