function prodImp(array) {
    var prod = 1;
    for (let i = 0; i < array.length; i++) {
        const element = array[i];
        if (element % 2 != 0) {
            prod *= element;
        } 
    }

    return prod;
}

function repeatChr(palabra, numero) {
    let resultado = ""
    for (let i = 0; i < palabra.length; i++) {
        const letra = palabra[i];
        let repeticiones = 1;
        while (repeticiones <= numero) {
            resultado += letra
            repeticiones += 1
        }
    }

    return resultado
}

function getInt(array) {
    let arrayFinal = [];
    for (let i = 0; i < array.length; i++) {
        const element = array[i];
        if (Number.isInteger(element) == true) {
            arrayFinal.push(element);
        }
    }

    return arrayFinal
}

const modulos = ["m02", "m03", "m04", "m05", "m06", "m07", "m08", "FOL"];
const alumnoNotas = [[8, 9, 4], [9, 10], [6, 8, 10], [4], [8, 4, 7], [], [7, 5, 9, 10],[10]];
const alumno = ["David", "López Fernandez", "DAWe", "29 / 04 / 2003"];

function cargaDatos() {
    const body = document.getElementsByTagName("body")[0]
    const contenedor = document.createElement("div")
    const nombre = document.createElement("h1");
    const curso = document.createElement("p");
    const cumpleaños = document.createElement("p");
    const textoNombre = document.createTextNode(alumno[0] + " " + alumno[1]);
    const textoCurso = document.createTextNode("Curso: " + alumno[2]);
    const textoCumpleaños = document.createTextNode("Cumpleaños: " + alumno[3]);
    nombre.appendChild(textoNombre);
    curso.appendChild(textoCurso);
    cumpleaños.appendChild(textoCumpleaños);
    contenedor.appendChild(nombre);
    contenedor.appendChild(curso);
    contenedor.appendChild(cumpleaños);
    body.appendChild(contenedor)
}

//Esta parte esta sin acabar
function cargaNotas() {
    const cuerpo = document.getElementsByTagName("body")[0];
    const contenedor = document.createElement("div");
    const titulo = document.createElement("h2");
    const textoTitulo = document.createTextNode("Notas");
    const moduleList = document.createElement("ul")
    titulo.appendChild(textoTitulo);
    contenedor.appendChild(titulo);
    cuerpo.appendChild(contenedor);
    
    for (let m = 0; m < modulos.length; m++) {
        const element = array[m];
        const modulo = document.createElement("li");
        const textoModulo = document.createTextNode(element);
        modulo.appendChild(textoModulo);
        moduleList.appendChild(modulo);
    }
    contenedor.appendChild(moduleList);
    cuerpo.appendChild(contenedor);
}

cargaDatos();
cargaNotas();