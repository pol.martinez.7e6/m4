<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/Videojuego">
        <article>
            <xsl:attribute name = "class">art_vid</xsl:attribute>
            <img class = "vid_img">
                <xsl:attribute name="src">
                        <xsl:value-of select="InfoGeneral/Foto/@foto"></xsl:value-of>
                </xsl:attribute>
            </img>
            <h2><xsl:value-of select = "Nombre"></xsl:value-of></h2>
            <p><b>Genero: </b><xsl:value-of select = "Genero"></xsl:value-of></p>
            <p><b>Disponible para: </b><xsl:value-of select = "Plataformas/Plataforma"></xsl:value-of></p>
            <p><b>Precio: </b><xsl:value-of select = "Plataformas/Plataforma/@precio"></xsl:value-of>$</p>
            <p><b>Fecha de salida: </b><xsl:value-of select = "Plataformas/Plataforma/@fechaSalida"></xsl:value-of></p>
            <p><b>Modos de juego: </b><xsl:value-of select = "ModosJuego/ModoJuego"></xsl:value-of></p>
        </article>
    </xsl:template>
</xsl:stylesheet>