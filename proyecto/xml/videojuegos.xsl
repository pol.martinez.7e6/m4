<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match = "/Videojuegos">
                <xsl:for-each select = "Videojuego">
                <article class = "art_vid">
                    <img class = "vid_img">
                        <xsl:attribute name="src">
                                <xsl:value-of select="InfoGeneral/Foto/@foto"></xsl:value-of>
                        </xsl:attribute>
                    </img>
                    <h2><xsl:value-of select = "Nombre"></xsl:value-of></h2>
                    <p><xsl:value-of select = "InfoGeneral/Descripcion"></xsl:value-of></p>
                    <a class = "a_main"><xsl:attribute name="href">
                        <xsl:value-of select="link"></xsl:value-of>
                    </xsl:attribute>Més info</a>

                </article>
                </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>